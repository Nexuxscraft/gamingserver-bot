import asyncio
import json
import os
from random import random

import discord
from discord import Member, guild, Guild
from discord.ext import commands


client = discord.Client()


@client.event
async def on_member_join(member):
    guild: Guild = member.guild
    if not member.bot:
        embed = discord.Embed(title='Will Kommen Auf Gamingservern.de>'.format(member.name),
                              description='Wir heißen dich herzlich Willkommen auf unserem Server!', color=0x22a7f0)
        try:
            if not member.dm_channel:
                await member.create_dm()
            await member.dm_channel.send(embed=embed)
        except discord.errors.Forbidden:
            print('Es konnte keine Willkommensnachricht an {} gesendet werden.'.format(member.name))
        autoguild = autoroles.get(guild.id)
        if autoguild and autoguild['692454365793091706']:
            for roleId in autoguild["692476603799699536"]:
                role = guild.get_role(roleId)
                if role:
                    await member.add_roles(role, reason='AutoRoles', atomic=True)
    else:
        autoguild = autoroles.get(guild.id)
        if autoguild and autoguild['691646154336894998']:
            for roleId in autoguild['694658629051351061']:
                role = guild.get_role(roleId)
                if role:
                    await member.add_roles(role, reason='AutoRoles', atomic=True)




def is_not_pinned(mess):
    return not mess.pinned


antworten =['Ja', 'Nein', 'Vielleicht', 'Wahrscheinlich', 'Sieht so aus', 'Sehr wahrscheinlich',
            'Sehr unwahrscheinlich']


autoroles = {
    658960248345853952: {'memberroles': [667129769397059596, 667129722995474460], 'botroles': [667129686131736586]},
    643782995869827092: {'memberroles': [], 'botroles': []}
}




async def status_task():
    while True:
        await client.change_presence(activity=discord.Game('Gamingservern.de'), status=discord.Status.online)
        await asyncio.sleep(3)
        await client.change_presence(activity=discord.Game('Bot Von'), status=discord.Status.online)
        await asyncio.sleep(3)
        await client.change_presence(activity=discord.Game('Nexuxscraft'), status=discord.Status.online)
        await asyncio.sleep(3)



@client.event
async def on_ready():
    print('Wir sind eingeloggt als User {}'.format(client.user.name))
    client.loop.create_task(status_task())




@client.event
async def on_message(message, is_not_pinned=None, antworten=None, var=None, true=None, console=None):
    if message.author.bot:
       return
    if ".help" in message.content:
        await message.channel.send("**.help GamingServern.De**\r\n"
                                   "!help - Zeigt unsere Hilfe\r\n"
                                   ".play könnt ihr Music abspielen Bald\r\n"
                                   ".8ball Sytsem beantwortet fragen\r\n"
                                   ".userinfo fragt eure User infos ab mit Z.B. .userinfo Nexuxscraft\r\n"
                                   ".Corona Zeigt Corona Zahlen in Deutschalnd Täglich aktuallisiert\r\n"
                                   "\r\n"
                                   "\r\n"
    if message.content.startswith('.clear'):
        if message.author.permissions_in(message.channel).manage_messages:
            args = message.content.split(' ')
            if len(args) == 2:
                if args[1].isdigit():
                    count = int(args[1]) + 1
                    deleted = await message.channel.purge(limit=count, check=is_not_pinned)
                    await message.channel.send('{} Nachrichten gelöscht.'.format(len(deleted)-1))
    if message.content.startswith(".ban"):
        args = message.content.split(' ')
        if len(args) == 2:
            member: Member = discord.utils.find(lambda m: args[1] in m.name, message.guild.members)
            if member:
                await member.ban()
                await message.channel.send(f"Der User {member.name} wurde gebannt")
            else:
                await message.channel.send(f"Kein Spieler Mit der ID{args[1]} wurde nicht gefunden")
    if message.content.startswith(".kick"):
        args = message.content.split(' ')
        if len(args) == 2:
            member: Member = discord.utils.find(lambda m: args[1] in m.name, message.guild.members)
            if member:
                await member.kick()
                await message.channel.send(f"Der User {member.name} wurde gekickt")
            else:
                await message.channel.send(f"Kein Spieler Mit der ID{args[1]} wurde nicht gefunden")
    if message.content.startswith(".unban"):
        args = message.content.split(' ')
        if len(args) == 2:
            member: Member = discord.utils.find(lambda m: args[1] in m.name, message.guild.members)
            if member:
                await member.unban()
                await message.channel.send(f"Der User {member.name} wurde entbannt Herzlich WIllkommen")
            else:
                await message.channel.send(f"Kein Spieler Mit der ID{args[1]} wurde nicht gefunden")

    if message.content.startswith('.userinfo'):
        args = message.content.split(' ')
        if len(args) == 2:
            member: Member = discord.utils.find(lambda m: args[1] in m.name, message.guild.members)
            if member:
                embed = discord.Embed(title='Userinfo für {}'.format(member.name),
                                      description='Dies ist eine Userinfo für den User {}'.format(member.mention),
                                      color=0x22a7f0)
                embed.add_field(name='Server beigetreten', value=member.joined_at.strftime('%d/%m/%Y, %H:%M:%S'),
                                inline=True)
                embed.add_field(name='Discord beigetreten', value=member.created_at.strftime('%d/%m/%Y, %H:%M:%S'),
                                inline=True)
                rollen = ''
                for role in member.roles:
                    if not role.is_default():
                        rollen += '{} \r\n'.format(role.mention)
                if rollen:
                    embed.add_field(name='Rollen', value=rollen, inline=True)
                embed.set_thumbnail(url=member.avatar_url)
                embed.set_footer(text='**GamingServern.De**')
                mess = await message.channel.send(embed=embed)
                await mess.add_reaction('🚍')
                await mess.add_reaction('')

    if message.content.startswith('.8ball'):
        args = message.content.split(' ')
        if len(args) >= 2:
            frage = ' '.join(args[1:])
            mess = await message.channel.send('Ich versuche deine Frage `{0}` zu beantworten.'.format(frage))
            await asyncio.sleep(2)
            await mess.edit(content='Ich kontaktiere das Orakel...')
            await asyncio.sleep(2)
            await mess.edit(content='Deine Antwort zur Frage `{0}` lautet: `{1}`'
                            .format(frage, random.choice(antworten)))

    if os.path.isfile("channels.json"):
        with open('channels.json', encoding='utf-8') as f:
            channels = json.load(f)
    else:
        channels = {}
        with open('channels.json', 'w') as f:
            json.dump(channels, f, indent=4)

    bot = commands.Bot(command_prefix=".")
    botcolor = 0xffffff

    bot.remove_command("help")

    # ---------------------------------------------------------------------

    tempchannels = [719639091024625787]
    admins = [691318794383261796, 693902211700359209, 694657502318428171, 691319239927398530]

    # ---------------------------------------------------------------------

    @bot.command(pass_context=True)
    async def addtempchannel(ctx, channelid):
        if ctx.author.bot:
            return
        if ctx.author.guild_permissions.administrator:
            if channelid:
                for vc in ctx.guild.voice_channels:
                    if vc.id == int(channelid):
                        if str(ctx.channel.guild.id) not in channels:
                            channels[str(ctx.channel.guild.id)] = []
                        channels[str(ctx.channel.guild.id)].append(int(channelid))
                        with open('channels.json', 'w') as f:
                            json.dump(channels, f, indent=4)
                        await ctx.send("{} ist nun ein JoinHub".format(vc.name))
                        return
                await ctx.send("{} ist kein Voicechannel".format(channelid))
            else:
                await ctx.send("Bitte gebe eine ChannelID an")
        else:
            await ctx.send("Du brauchst das Recht Administrator um das zu tun")

    @bot.command(pass_context=True)
    async def removetempchannel(ctx, channelid):
        if ctx.author.bot:
            return
        if ctx.author.guild_permissions.administrator:
            if channelid:
                guildS = str(ctx.channel.guild.id)
                channelidI = int(channelid)
                for vc in ctx.guild.voice_channels:
                    if vc.id == int(channelid):
                        if channels[guildS]:
                            if channelidI in channels[guildS]:
                                channels[guildS].remove(channelidI)
                                with open('channels.json', 'w') as f:
                                    json.dump(channels, f, indent=4)
                                    await ctx.send("{} ist kein JoinHub mehr".format(vc.name))
                                    return
                            else:
                                await ctx.send("Dieser Channel existiert hier nicht")
                                return
                await ctx.send("Du besitzt noch keine JoinHubs")
            else:
                await ctx.send("Keine Channelid angegeben")
        else:
            await ctx.send("Du brauchst das Recht Administrator um das zu tun")

    @bot.command(pass_context=True)
    async def info(ctx):
        if ctx.author.bot:
            return
        if ctx.author.id in admins:
            membercount = 0
            guildcount = 0
            for guild in bot.guilds:
                membercount += guild.member_count
                guildcount += 1
            embed = discord.Embed(title='Informationen',
                                  description=f'Der Bot ist derzeit auf {guildcount - 1} anderen '
                                              f'Servern und sieht {membercount} Members.',
                                  color=0xfefefe)
            await ctx.channel.send(embed=embed)

    @bot.command(pass_context=True)
    async def help(ctx):
        if ctx.author.bot:
            return
        embed = discord.Embed(title='Befehle für die Tempchannel', description=f'.addtempchannel <channelid> - '
                                                                               f'Fügt den Channel als Tempchannel hinzu.\n'
                                                                               f'.removetempchannel <channelid> - '
                                                                               f'Entfernt den Tempchannel.',
                              color=0xfefefe)
        if ctx.author.id in admins:
            embed.add_field(name='Adminbefehle', value='.info - Zeigt allgemeine Statistiken.\n'
                                                       '.leave <guildid> - Der Bot leaved von der Guild.', inline=False)
        await ctx.channel.send(embed=embed)

    @bot.command(pass_context=True)
    async def leave(ctx, serverid):
        if ctx.author.bot:
            return
        if ctx.author.id in admins:
            guild = bot.get_guild(serverid)
            if guild:
                await ctx.channel.send(f'{guild.name} geleaved.')
                await guild.leave()
            else:
                await ctx.channel.send(f'Keine Guild mit der ID {serverid} gefunden.')

    # ---------------------------------------------------------------------
    @bot.event
    async def on_voice_state_update(member, before, after):
        if before.channel:
            if isTempChannel(before.channel):
                bchan = before.channel
                if len(bchan.members) == 0:
                    await bchan.delete(reason="No member in tempchannel")
        if after.channel:
            if isJoinHub(after.channel):
                overwrite = discord.PermissionOverwrite()
                overwrite.manage_channels = True
                overwrite.move_members = True
                name = "│⏳ {}".format(member.name)
                output = await after.channel.clone(name=name, reason="Joined in joinhub")
                if output:
                    tempchannels.append(output.id)
                    await output.set_permissions(member, overwrite=overwrite)
                    await member.move_to(output, reason="Created tempvoice")

    # ---------------------------------------------------------------------

    async def getChannel(guild, name):
        for channel in guild.voice_channels:
            if name in channel.name:
                return channel
        return None

    def isJoinHub(channel):
        if channels[str(channel.guild.id)]:
            if channel.id in channels[str(channel.guild.id)]:
                return True
        return False

    def isTempChannel(channel):
        if channel.id in tempchannels:
            return True
        else:
            return False


client.run("Your Bot")
Bitte Nexuxscraft Stehen lassen Copirighyt Nexuxcraft 2020

Wir haben noch mehr Commands nur wir haben unsere Rausgemacht
