client.event
async def on_member_join(member):
    if not member.bot:
        embed = discord.Embed(title='Willkomman Auf Gamingservern'.format(member.name),
                              description='Wir heißen dich hier auf dem Server herzlich Willkommen',
                              color=0x22a7f0)
        try:
            if not member.dm_channel:
                await member.create_dm()
            await member.dm_channel.send(embed=embed);
        except discord.errors.Forbidden:
            print('Es konnte keine Willkommensnachricht an {0} gesendet werden.'.format(member.name))



def is_not_pinned(mess):
    return not mess.pinned

if message.content.startswith('.clear'):
    if message.author.permissions_in(message.channel).manage_messages:
        args = message.content.split(' ')
        if len(args) == 2:
            if args[1].isdigit():
                count = int(args[1])+1
                deleted = await message.channel.purge(limit=count, check=is_not_pinned)
                await message.channel.send('{} Nachrichten gelscht.'.format(len(deleted)-1))

    import random
    from discord import Member, Guild

    antworten = ['Ja', 'Nein', 'Vielleicht', 'Wahrscheinlich', 'Sieht so aus', 'Sehr wahrscheinlich',
                 'Sehr unwahrscheinlich']


    #  on_message(message)
    if message.content.startswith('.userinfo'):
        args = message.content.split(' ')
        if len(args) == 2:
            member: Member = discord.utils.find(lambda m: args[1] in m.name, message.guild.members)
            if member:
                embed = discord.Embed(title='Userinfo für {}'.format(member.name),
                                      description='Dies ist die Userinfo für den User {}'.format(member.mention),
                                      color=0x22a7f0)
                embed.add_field(name='Server beigetreten', value=member.joined_at.strftime("%m/%d/%Y, %H:%M:%S"),
                                inline=True)
                embed.add_field(name='Discord beigetreten', value=member.created_at.strftime("%m/%d/%Y, %H:%M:%S"),
                                inline=True)
                rollen = ''
                for role in member.roles:
                    if not role.is_default():
                        rollen += '{} \r\n'.format(role.mention)
                if rollen:
                    embed.add_field(name='Rollen', value=rollen, inline=True)
                embed.set_thumbnail(url=message.author.avatar_url)
                embed.set_footer(text='Ich bin ein EmbedFooter!')
                message = await message.channel.send(embed=embed)
                await message.add_reaction('🚍')
                await message.add_reaction('')
    @client.event
    async def on_member_join(member):
        if not member.bot:
            embed = discord.Embed(title='Willkommen auf Gamingservern.de {} <a:tut_herz:662606955520458754>'.format(member.name),
                                  description='Wir heißen dich hier auf dem Server herzlich Willkommen',
                                  color=0x22a7f0)
            try:
                if not member.dm_channel:
                    await member.create_dm()
                await member.dm_channel.send(embed=embed);
            except discord.errors.Forbidden:
                print('Es konnte keine Willkommensnachricht an {0} gesendet werden.'.format(member.name))


    antworten = ['Ja', 'Nein', 'Vielleicht', 'Wahrscheinlich', 'Sieht so aus', 'Sehr wahrscheinlich',
                 'Sehr unwahrscheinlich']

    colors = [discord.Colour.red(), discord.Colour.orange(), discord.Colour.gold(), discord.Colour.green(),
              discord.Colour.blue(), discord.Colour.purple()]
    if client.get_guild(719582338203517049):
        guild: Guild = client.get_guild(691318393013403709)
        role = guild.get_role(692454365793091706)
        if role:
            if role.position < guild.get_member(client.user.id).top_role.position:
                await role.edit(colour=random.choice(colors))

        if message.content.startswith('.8ball'):
            args = message.content.split(' ')
            if len(args) >= 2:
                frage = ' '.join(args[1:])
                mess = await message.channel.send('Ich versuche deine Frage `{0}` zu beantworten.'.format(frage))
                await asyncio.sleep(2)
                await mess.edit(content='Ich kontaktiere das Orakel...')
                await asyncio.sleep(2)
                await mess.edit(content='Deine Antwort zur Frage `{0}` lautet: `{1}`'
                                .format(frage, random.choice(antworten)))

